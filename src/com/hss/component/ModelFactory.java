/**
 * This file is part of iDempiere ERP <http://www.idempiere.org>.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 * 
 * Copyright (C) 2015 INGEINT <http://www.ingeint.com>.
 * Copyright (C) Contributors.
 * 
 * Contributors:
 *    - 2015 Saúl Piña <spina@ingeint.com>.
 */

package com.hss.component;

import com.hss.base.CustomModelFactory;
import com.hss.model.*;

/**
 * Model Factory
 */
public class ModelFactory extends CustomModelFactory {

	/**
	 * For initialize class. Register the models to build
	 * 
	 * <pre>
	 * protected void initialize() {
	 * 	registerTableModel(MTableExample.Table_Name, MTableExample.class);
	 * }
	 * </pre>
	 */
	@Override
	protected void initialize() {
		// registerTableModel(com.hss.model.MTableExample.Table_Name, com.hss.model.MTableExample.class);
		registerTableModel(MCupon.Table_Name, MCupon.class);
		registerTableModel(MRegAndroid.Table_Name, MRegAndroid.class);
		registerTableModel(MConfiguracionOrden.Table_Name, MConfiguracionOrden.class);
		registerTableModel(MProductColor.Table_Name, MProductColor.class);
		registerTableModel(MProductSize.Table_Name, MProductSize.class);
	}

}
