package com.hss.process;

import java.math.BigDecimal;
import java.util.Properties;
import java.util.logging.Level;
import java.util.regex.Pattern;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBPartner;
import org.compiere.model.MDocType;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MOrg;
import org.compiere.model.MPriceList;
import org.compiere.model.MSequence.GetIDs;
import org.compiere.model.MWarehouse;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.util.Env;
import org.compiere.util.Msg;

import com.hss.base.CustomProcess;
import com.hss.model.MConfiguracionOrden;
import com.hss.model.MProductColor;
import com.hss.model.MProductSize;
import com.hss.util.TimestampUtil;

public class RegOrder extends CustomProcess {
	int C_BPartner_ID;
	String Description;

	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null);
			else if (name.equals("C_BPartner_ID"))
				C_BPartner_ID = para[i].getParameterAsInt();
			else if (name.equals("Description"))
				Description = para[i].getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}	
	}

	@Override
	protected String doIt() throws Exception {
		MConfiguracionOrden ConfigOrder = getConfigOrder(getCtx(), get_TrxName());
		
		MOrder order = new MOrder(getCtx(), 0, get_TrxName());
		order.setAD_Org_ID(ConfigOrder.getAD_Org_ID());
		order.setC_DocType_ID(0);
		order.setC_DocTypeTarget_ID(ConfigOrder.getC_DocType_ID());
		order.setDateAcct(TimestampUtil.now());
		order.setDateOrdered(TimestampUtil.now());
		order.setBPartner(getBPartner(getCtx(), C_BPartner_ID, get_TrxName()));
		order.setSalesRep_ID(getBPartner(getCtx(), C_BPartner_ID, get_TrxName()).getSalesRep_ID());
		order.setM_PriceList_ID(ConfigOrder.getM_PriceList_ID());
		order.setM_Warehouse_ID(ConfigOrder.getM_Warehouse_ID());
		order.setDescription("Orden ProMovil");
		order.setIsSOTrx(true);
		order.saveEx();
		
		int mLine = 10;
		int M_Product_ID;
		BigDecimal QtyEntered;
		int HSS_ProductColor_ID;
		int HSS_ProductSize_ID;
		
		String[] lines = Description.split(Pattern.quote("|"));
		
		for (String line : lines) {
			String[] elements = line.split(";");
			M_Product_ID = Integer.parseInt( elements[0] );
			QtyEntered = new BigDecimal( elements[1] );

			MOrderLine orderline = new MOrderLine(getCtx(), 0, get_TrxName());
			orderline.setC_Order_ID(order.getC_Order_ID());
			orderline.setAD_Org_ID(order.getAD_Org_ID());
			orderline.setLine(mLine);
			orderline.setM_Product_ID(M_Product_ID);
			orderline.setQtyEntered(QtyEntered);
			orderline.setQtyOrdered(QtyEntered);
			
			if (elements.length == 4) {
				HSS_ProductColor_ID = Integer.parseInt( elements[2] );
				HSS_ProductSize_ID = Integer.parseInt( elements[3] );

				MProductColor mProductColor = new MProductColor(getCtx(), HSS_ProductColor_ID, get_TrxName());
				MProductSize mProductSize = new MProductSize(getCtx(), HSS_ProductSize_ID, get_TrxName());

				orderline.set_ValueOfColumn("HSS_Color_ID", mProductColor.getHSS_Color_ID());
				orderline.set_ValueOfColumn("HSS_Size_ID", mProductSize.getHSS_Size_ID());
			}

			orderline.saveEx();
			
			mLine += 10;
		}
		
	    order.prepareIt();
	    order.setDocAction(DocAction.ACTION_Complete);
	    String StrTemp = order.completeIt();
	    if(!StrTemp.equals(DocAction.ACTION_Complete)) {
			throw new AdempiereException(Msg.getMsg(getCtx(), order.getProcessMsg()));
	    }
	    order.setDocStatus(StrTemp);
	    order.setDocAction(StrTemp);
	    order.saveEx();
	    order.setProcessed(true);

		addBufferLog(order.get_ID(), order.getDateAcct(), null, "Orden Generada: " + order.getDocumentNo(),
				order.get_Table_ID(), order.get_ID());
		
		return order.getDocumentNo();
	}

	public static MBPartner getBPartner(Properties ctx, int taxid, String trxName) {
		final String whereClause = "C_BPartner_ID = ? ";
		MBPartner retValue = new Query(ctx, MBPartner.Table_Name, whereClause, trxName)
				.setParameters(taxid)
				.setClient_ID()
				.firstOnly();
		return retValue;
	}

	public static MConfiguracionOrden getConfigOrder(Properties ctx, String trxName) {
		final String whereClause = "IsActive = 'Y' ";
		MConfiguracionOrden retValue = new Query(ctx, MConfiguracionOrden.Table_Name, whereClause, trxName)
				.setClient_ID()
				.firstOnly();
		return retValue;
	}

}
