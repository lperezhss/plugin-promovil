package com.hss.process;

import java.math.BigDecimal;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MLocation;
import org.compiere.model.MUser;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.hss.base.CustomProcess;

public class RegPartner extends CustomProcess {

	int pTId = 0;
	String pRUC = null;
	String pRazonSocial = null;
	String pName1 = null;
	String pName2 = null;
	String pLastName1 = null;
	String pLastName2 = null;
	int pSalesRep = 0;
	String pTelefono = null;
	String pEMail = null;
	String pDirMatriz = null;
	
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("LCO_TaxIdType_ID"))
				pTId = para[i].getParameterAsInt();
			else if (name.equals("Value"))
				pRUC = para[i].getParameterAsString();
			else if (name.equals("Name"))
				pRazonSocial = para[i].getParameterAsString();
			else if (name.equals("FirstName1"))
				pName1 = para[i].getParameterAsString();
			else if (name.equals("FirstName2"))
				pName2 = para[i].getParameterAsString();
			else if (name.equals("LastName1"))
				pLastName1 = para[i].getParameterAsString();
			else if (name.equals("LastName2"))
				pLastName2 = para[i].getParameterAsString();
			else if (name.equals("SalesRep_ID"))
				pSalesRep = para[i].getParameterAsInt();
			else if (name.equals("Phone"))
				pTelefono = para[i].getParameterAsString();
			else if (name.equals("EMail"))
				pEMail = para[i].getParameterAsString();
			else if (name.equals("Address1"))
				pDirMatriz = para[i].getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception {
		MBPartner partner = new Query(getCtx(), MBPartner.Table_Name, MBPartner.COLUMNNAME_TaxID + " = ?", get_TrxName())
				.setClient_ID()
				.setOnlyActiveRecords(true)
				.setParameters(new Object[] {pRUC})
				.first();
		
		if (partner == null) {
			partner = new MBPartner(getCtx(), 0, get_TrxName());
		}
		partner.setAD_Org_ID(0);
		partner.set_ValueOfColumn("LCO_TaxIdType_ID", pTId);
		partner.setTaxID(pRUC);
		partner.setValue(pRUC);
		if (pRazonSocial == null || pRazonSocial.equals("")) {
			partner.setName(pName1 + " " + pName2 + ", " + pLastName1 + " " + pLastName2);
			partner.set_ValueNoCheck("FirstName1", pName1);
			partner.set_ValueNoCheck("FirstName2", pName2);
			partner.set_ValueNoCheck("LastName1", pLastName1);
			partner.set_ValueNoCheck("LastName2", pLastName2);
		} else {
			partner.setName(pRazonSocial);
		}
		partner.setIsCustomer(true);
		partner.setIsVendor(true);
		partner.setSalesRep_ID(pSalesRep);
		
		String taxIdType_Name = getTaxIdType_Name(getCtx(), pTId, get_TrxName());
		
		if (!taxIdType_Name.isEmpty()) {
			int TaxPayerType_ID = 0;
			
			if (taxIdType_Name.equals("C") || 
					taxIdType_Name.equals("P") || 
					taxIdType_Name.equals("E") || 
					taxIdType_Name.equals("F")) {
				TaxPayerType_ID = getTaxPayerType_ID(getCtx(), "01", get_TrxName());
			} else if (taxIdType_Name.equals("R")) {
				TaxPayerType_ID = getTaxPayerType_ID(getCtx(), "02", get_TrxName());
			}
			
			if (TaxPayerType_ID != 0) {
				partner.set_ValueOfColumn("LCO_TaxPayerType_ID", TaxPayerType_ID);
			}
		}
		partner.saveEx();
		
		MUser user = new Query(getCtx(), MUser.Table_Name, MUser.COLUMNNAME_C_BPartner_ID + " = ?", get_TrxName())
				.setClient_ID()
				.setOnlyActiveRecords(true)
				.setParameters(new Object[] {partner.getC_BPartner_ID()})
				.first();
		
		if (user == null) {
			user = new MUser(partner);
			user.setPhone(pTelefono);
			user.setEMail(pEMail);
			user.saveEx();
		} else {
			user.setPhone(pTelefono);
			user.setEMail(pEMail);
			user.saveEx();
		}
		
		
		MBPartnerLocation partnerLocation = new Query(getCtx(), MBPartnerLocation.Table_Name, MBPartnerLocation.COLUMNNAME_C_BPartner_ID + " = ?", get_TrxName())
				.setClient_ID()
				.setOnlyActiveRecords(true)
				.setParameters(new Object[] {partner.getC_BPartner_ID()})
				.first();
		
		if (partnerLocation == null) {
			MLocation location = new MLocation(getCtx(), 0, get_TrxName());
			location.setAddress1(pDirMatriz);
			location.saveEx();
			
			partnerLocation = new MBPartnerLocation(partner);
			partnerLocation.setC_Location_ID(location.getC_Location_ID());
			partnerLocation.saveEx();
		} else {
			MLocation location = new MLocation(getCtx(), partnerLocation.getC_Location_ID(), get_TrxName());
			location.setAddress1(pDirMatriz);
			location.saveEx();	
		}
		
		
		return String.valueOf(partner.getC_BPartner_ID());
	}

	public static String getTaxIdType_Name(Properties ctx, int vLCO_TaxIdType_ID, String trxName) {
		String TaxIdType = DB.getSQLValueString(trxName, 
				"select Name "
				+ "from LCO_TaxIdType "
				+ "where (AD_Client_ID = ? or AD_Client_ID = 0)"
				+ "and LCO_TaxIdType_ID = ? "
				+ "order by name", Env.getAD_Client_ID(ctx), vLCO_TaxIdType_ID);
		return TaxIdType.substring(0, 1);
	}

	public static int getTaxPayerType_ID(Properties ctx, String Value, String trxName) {
		BigDecimal TaxPayerType = DB.getSQLValueBD(trxName, 
				"select LCO_TaxPayerType_ID "
				+ "from LCO_TaxPayerType "
				+ "where (AD_Client_ID = ? or AD_Client_ID = 0)"
				+ "and value = ? "
				+ "order by value", Env.getAD_Client_ID(ctx), Value);
		return TaxPayerType.intValue();
	}
}
