package com.hss.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.logging.Level;

import org.adempiere.exceptions.DBException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.util.DB;

import com.hss.base.CustomProcess;

public class TestGenerateModel extends CustomProcess {

	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null);
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception {
		String sql = "select * from m_product";
		createColumns(sql);
		
		return null;
	}
	
	private StringBuilder createColumns(String sql) {
		StringBuilder sb = new StringBuilder();
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, null);
			rs = pstmt.executeQuery();
			
			String sourceFolder = "/tmp/";
			String packageName = "com.hss.process";
			String modelName = "Prueba";

			new GenerateModel(sourceFolder, packageName, modelName, rs);
		} catch (SQLException e) {
			throw new DBException(e, sql);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return sb;
	}

}
