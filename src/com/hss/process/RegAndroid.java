package com.hss.process;

import java.util.logging.Level;

import org.compiere.model.MUser;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;

import com.hss.base.CustomProcess;
import com.hss.model.MRegAndroid;

public class RegAndroid extends CustomProcess {
	String AndroidID;
	int SalesRep_ID;

	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null);
			else if (name.equals("AndroidID"))
				AndroidID = para[i].getParameterAsString();
			else if (name.equals("SalesRep_ID"))
				SalesRep_ID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}	
	}

	@Override
	protected String doIt() throws Exception {
		MRegAndroid regAndroid = new Query(getCtx() , MRegAndroid.Table_Name , "Value = ? ", get_TrxName())
				.setClient_ID()
				.setOnlyActiveRecords(true)
				.setParameters(new Object[] {AndroidID})
				.setOrderBy(MRegAndroid.COLUMNNAME_HSS_RegAndroid_ID)
				.first();
		
		if (regAndroid == null) {
			regAndroid = new MRegAndroid(getCtx(), 0, get_TrxName());
			regAndroid.setValue(AndroidID);
			regAndroid.setName(AndroidID);
			regAndroid.setSalesRep_ID(SalesRep_ID);
			regAndroid.setHSS_Status(MRegAndroid.HSS_STATUS_Certificado);
			regAndroid.saveEx();
		} else {
			if(regAndroid.getHSS_Status().equals(MRegAndroid.HSS_STATUS_Pendiente)) {
				if (com.hss.util.TimestampUtil.between(regAndroid.getCreated()) > 15) {
					regAndroid.setHSS_Status(MRegAndroid.HSS_STATUS_Vencido);
					regAndroid.saveEx();
				}
			}
		}
		
		return regAndroid.getHSS_Status();
	}

}
