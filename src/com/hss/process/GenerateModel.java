package com.hss.process;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.TreeSet;


public class GenerateModel {
	private Collection<String> s_importClasses = new TreeSet<String>();
	public static final String NL = "\n";
	
	public GenerateModel(String directory, String packageName, String modelName, ResultSet rs) {
		// create column access methods
		// tringBuilder mandatory = new StringBuilder();
		StringBuilder sb = createColumns(packageName, rs);

		// Header
		createHeader(modelName, packageName, sb);

		// Save
		if (directory.endsWith("/") || directory.endsWith("\\")) {
		} else {
			directory = directory + File.separator;
		}
		
		writeToFile(sb, directory + modelName + ".java");
	}
	
	private StringBuilder createColumns(String packageName, ResultSet rs) {
		StringBuilder sb = new StringBuilder();
		try {
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnCount = rsmd.getColumnCount();

			// The column count starts from 1
			for (int i = 1; i <= columnCount; i++ ) {
				String columnName = rsmd.getColumnName(i);
				String columnClass = rsmd.getColumnClassName(i);

				// Create COLUMNNAME_ property (teo_sarca, [ 1662447 ])
				sb.append(NL)
					.append("\t/** Column name ").append(columnName).append(" */").append(NL)
					.append("\tpublic static final String COLUMNNAME_").append(columnName)
					.append(" = \"").append(columnName)
					.append("\";").append(NL);

				//
				sb.append(createColumnMethods(packageName, columnName, columnClass));
			}
		} catch (SQLException e) {
		}
		
		return sb;
	}
	
	private String createHeader(String modelName, String packageName, StringBuilder sb) {
		StringBuilder start = new StringBuilder().append("package ").append(packageName).append(";")
				.append(NL);
		
		addImportClass(java.math.BigDecimal.class, packageName);

		createImports(start);
		start.append(NL);
		start.append("@SuppressWarnings(\"all\")\n");
		start.append("public ").append(modelName).append(" {").append(NL);

		String end = "}";
		//
		sb.insert(0, start);
		sb.append(end);

		return modelName;
	}
	
	private String createColumnMethods(String packageName, String columnName, String columnClass) {
		Class<?> clazz = getClass(columnClass);
		String dataType = getDataTypeName(clazz);

		StringBuilder sb = new StringBuilder();

		// ****** Set Comment ******
		generateJavaComment("Set", columnName, "", sb);
		sb.append("\tpublic void set").append(columnName)
			.append("(").append(dataType).append(" ").append(columnName).append(") {").append(NL)
			.append("\t\tthis.").append(columnName).append(" = ").append(columnName).append(";").append(NL)
			.append("\t}").append(NL);
		
		
		// ****** Get Comment ******
		generateJavaComment("Get", columnName, "", sb);
		if (clazz.equals(Boolean.class)) {
			sb.append("\tpublic ").append(dataType);
			sb.append(" is");
			if (columnName.toLowerCase().startsWith("is"))
				sb.append(columnName.substring(2));
			else
				sb.append(columnName);
			sb.append("();").append(NL);
		} else {
			sb.append("\tpublic ").append(dataType).append(" get").append(columnName)
				.append("() {").append(NL)
				.append("\t\treturn this.").append(columnName).append(";").append(NL)
				.append("\t}").append(NL);
		}
		sb.append(NL);
		
		addImportClass(clazz, packageName);
		return sb.toString();
	}

	/**
	 * Get class for given display type and reference
	 * 
	 * @param displayType
	 * @param AD_Reference_ID
	 * @return class
	 */
	public static Class<?> getClass(String columnClass) {
		if (columnClass.equals("java.sql.Timestamp")) {
			return Timestamp.class;
		} else if(columnClass.equals("java.math.BigDecimal")) {
			return BigDecimal.class;
		} else {
			return String.class;
		}
	}

	public static String getDataTypeName(Class<?> cl) {
		String dataType = cl.getName();
		dataType = dataType.substring(dataType.lastIndexOf('.') + 1);
		if (dataType.equals("Boolean")) {
			dataType = "boolean";
		} else if (dataType.equals("Integer")) {
			dataType = "int";
		}
		return dataType;
	}

	// ****** Set/Get Comment ******
	public void generateJavaComment(String startOfComment, String propertyName, String description,
			StringBuilder result) {
		result.append(NL).append("\t/** ").append(startOfComment).append(" ").append(propertyName);

		if (description != null && description.length() > 0)
			result.append(".\n\t  * ").append(description).append(NL);

		result.append("\t  */\n");
	}

	public static String getFieldName(String columnName) {
		String fieldName;
		if (columnName.endsWith("_ID_To"))
			fieldName = columnName.substring(0, columnName.length() - 6) + "_To";
		else
			fieldName = columnName.substring(0, columnName.length() - 3);
		return fieldName;
	}
	
	private void createImports(StringBuilder sb) {
		sb.append(NL);
		for (String name : s_importClasses) {
			sb.append("import ").append(name).append(";").append(NL); // .append(NL);
		}
		sb.append(NL);
	}
	
	private void addImportClass(Class<?> cl, String packageName) {
		if (cl.isArray()) {
			cl = cl.getComponentType();
		}
		if (cl.isPrimitive())
			return;
		addImportClass(cl.getCanonicalName(), packageName);
	}
	
	private void addImportClass(String className, String packageName) {
		if (className == null || (className.startsWith("java.lang.") && !className.startsWith("java.lang.reflect."))
				|| className.startsWith(packageName + "."))
			return;
		for (String name : s_importClasses) {
			if (className.equals(name))
				return;
		}
		if (className.equals("byte[]")) {
			return;
		}
		s_importClasses.add(className);
	}
	
	private void writeToFile(StringBuilder sb, String fileName) {
		try {
			File out = new File(fileName);
			Writer fw = new OutputStreamWriter(new FileOutputStream(out, false), "UTF-8");
			for (int i = 0; i < sb.length(); i++) {
				char c = sb.charAt(i);
				fw.write(c);
			}
			fw.flush();
			fw.close();
			float size = out.length();
			size /= 1024;
			StringBuilder msgout = new StringBuilder().append(out.getAbsolutePath()).append(" - ").append(size)
					.append(" kB");
			System.out.println(msgout.toString());
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
}