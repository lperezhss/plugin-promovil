package com.hss.event;

import com.hss.base.CustomEventHandler;
import com.hss.model.MCupon;

public class Cupon_GenValue extends CustomEventHandler {

	@Override
	protected void doHandleEvent() {
		MCupon cupon = (MCupon) getPO();
		String newValue = cupon.getHSS_Cupon_UU().replace("-", "").substring(0, 4).toUpperCase() + "-" + cupon.getHSS_Cupon_UU().replace("-", "").substring(4, 8).toUpperCase();
		if (!cupon.getValue().equals(newValue)) {
			cupon.setValue(newValue);
			cupon.saveEx();
		}
	}

}
