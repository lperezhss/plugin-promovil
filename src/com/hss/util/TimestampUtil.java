package com.hss.util;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;

public class TimestampUtil {

	public static Timestamp now() {
		return new Timestamp(Calendar.getInstance().getTimeInMillis());
	}
	
	public static int between(Timestamp from) {
		return between(from, now());
	}
	
	public static int between(Timestamp from, Timestamp to) {
		LocalDateTime dateFrom = from.toInstant()
	              .atZone(ZoneId.systemDefault())
	              .toLocalDateTime();
		
		LocalDateTime dateTo = to.toInstant()
	              .atZone(ZoneId.systemDefault())
	              .toLocalDateTime();
		
		Duration d = Duration.between(dateFrom, dateTo);
		
		return (int) d.toDays();
	}
}
