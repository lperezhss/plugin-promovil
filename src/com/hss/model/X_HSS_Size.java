/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.hss.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for HSS_Size
 *  @author iDempiere (generated) 
 *  @version Release 5.1 - $Id$ */
public class X_HSS_Size extends PO implements I_HSS_Size, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20191027L;

    /** Standard Constructor */
    public X_HSS_Size (Properties ctx, int HSS_Size_ID, String trxName)
    {
      super (ctx, HSS_Size_ID, trxName);
      /** if (HSS_Size_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_HSS_Size (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 2 - Client 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_HSS_Size[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Size.
		@param HSS_Size Size	  */
	public void setHSS_Size (BigDecimal HSS_Size)
	{
		set_Value (COLUMNNAME_HSS_Size, HSS_Size);
	}

	/** Get Size.
		@return Size	  */
	public BigDecimal getHSS_Size () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_HSS_Size);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Size.
		@param HSS_Size_ID Size	  */
	public void setHSS_Size_ID (int HSS_Size_ID)
	{
		if (HSS_Size_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_HSS_Size_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_HSS_Size_ID, Integer.valueOf(HSS_Size_ID));
	}

	/** Get Size.
		@return Size	  */
	public int getHSS_Size_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_HSS_Size_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set HSS_Size_UU.
		@param HSS_Size_UU HSS_Size_UU	  */
	public void setHSS_Size_UU (String HSS_Size_UU)
	{
		set_ValueNoCheck (COLUMNNAME_HSS_Size_UU, HSS_Size_UU);
	}

	/** Get HSS_Size_UU.
		@return HSS_Size_UU	  */
	public String getHSS_Size_UU () 
	{
		return (String)get_Value(COLUMNNAME_HSS_Size_UU);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}