/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.hss.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for HSS_Color
 *  @author iDempiere (generated) 
 *  @version Release 5.1 - $Id$ */
public class X_HSS_Color extends PO implements I_HSS_Color, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20191027L;

    /** Standard Constructor */
    public X_HSS_Color (Properties ctx, int HSS_Color_ID, String trxName)
    {
      super (ctx, HSS_Color_ID, trxName);
      /** if (HSS_Color_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_HSS_Color (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 2 - Client 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_HSS_Color[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Color.
		@param HSS_Color_ID Color	  */
	public void setHSS_Color_ID (int HSS_Color_ID)
	{
		if (HSS_Color_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_HSS_Color_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_HSS_Color_ID, Integer.valueOf(HSS_Color_ID));
	}

	/** Get Color.
		@return Color	  */
	public int getHSS_Color_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_HSS_Color_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set HSS_Color_UU.
		@param HSS_Color_UU HSS_Color_UU	  */
	public void setHSS_Color_UU (String HSS_Color_UU)
	{
		set_ValueNoCheck (COLUMNNAME_HSS_Color_UU, HSS_Color_UU);
	}

	/** Get HSS_Color_UU.
		@return HSS_Color_UU	  */
	public String getHSS_Color_UU () 
	{
		return (String)get_Value(COLUMNNAME_HSS_Color_UU);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}