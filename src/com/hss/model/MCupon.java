package com.hss.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MCupon extends X_HSS_Cupon {

	private static final long serialVersionUID = -8870109922889731925L;

	public MCupon(Properties ctx, int HSS_Cupon_ID, String trxName) {
		super(ctx, HSS_Cupon_ID, trxName);
	}

	public MCupon(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

}
