/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.hss.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for HSS_Cupon
 *  @author iDempiere (generated) 
 *  @version Release 5.1 - $Id$ */
public class X_HSS_Cupon extends PO implements I_HSS_Cupon, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20191027L;

    /** Standard Constructor */
    public X_HSS_Cupon (Properties ctx, int HSS_Cupon_ID, String trxName)
    {
      super (ctx, HSS_Cupon_ID, trxName);
      /** if (HSS_Cupon_ID == 0)
        {
			setHSS_Client (0);
			setHSS_Org (0);
			setHSS_Role (0);
			setHSS_SalesRep (0);
        } */
    }

    /** Load Constructor */
    public X_HSS_Cupon (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 2 - Client 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_HSS_Cupon[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Client.
		@param HSS_Client Client	  */
	public void setHSS_Client (int HSS_Client)
	{
		set_Value (COLUMNNAME_HSS_Client, Integer.valueOf(HSS_Client));
	}

	/** Get Client.
		@return Client	  */
	public int getHSS_Client () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_HSS_Client);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set HSS_Cupon.
		@param HSS_Cupon_ID HSS_Cupon	  */
	public void setHSS_Cupon_ID (int HSS_Cupon_ID)
	{
		if (HSS_Cupon_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_HSS_Cupon_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_HSS_Cupon_ID, Integer.valueOf(HSS_Cupon_ID));
	}

	/** Get HSS_Cupon.
		@return HSS_Cupon	  */
	public int getHSS_Cupon_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_HSS_Cupon_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set HSS_Cupon_UU.
		@param HSS_Cupon_UU HSS_Cupon_UU	  */
	public void setHSS_Cupon_UU (String HSS_Cupon_UU)
	{
		set_ValueNoCheck (COLUMNNAME_HSS_Cupon_UU, HSS_Cupon_UU);
	}

	/** Get HSS_Cupon_UU.
		@return HSS_Cupon_UU	  */
	public String getHSS_Cupon_UU () 
	{
		return (String)get_Value(COLUMNNAME_HSS_Cupon_UU);
	}

	/** Set Org.
		@param HSS_Org Org	  */
	public void setHSS_Org (int HSS_Org)
	{
		set_Value (COLUMNNAME_HSS_Org, Integer.valueOf(HSS_Org));
	}

	/** Get Org.
		@return Org	  */
	public int getHSS_Org () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_HSS_Org);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Role.
		@param HSS_Role Role	  */
	public void setHSS_Role (int HSS_Role)
	{
		set_Value (COLUMNNAME_HSS_Role, Integer.valueOf(HSS_Role));
	}

	/** Get Role.
		@return Role	  */
	public int getHSS_Role () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_HSS_Role);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set SalesRep.
		@param HSS_SalesRep SalesRep	  */
	public void setHSS_SalesRep (int HSS_SalesRep)
	{
		set_Value (COLUMNNAME_HSS_SalesRep, Integer.valueOf(HSS_SalesRep));
	}

	/** Get SalesRep.
		@return SalesRep	  */
	public int getHSS_SalesRep () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_HSS_SalesRep);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set URL.
		@param URL 
		Full URL address - e.g. http://www.idempiere.org
	  */
	public void setURL (String URL)
	{
		set_Value (COLUMNNAME_URL, URL);
	}

	/** Get URL.
		@return Full URL address - e.g. http://www.idempiere.org
	  */
	public String getURL () 
	{
		return (String)get_Value(COLUMNNAME_URL);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}