package com.hss.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MProductSize extends X_HSS_ProductSize {

	private static final long serialVersionUID = 759027224219170730L;

	public MProductSize(Properties ctx, int HSS_ProductSize_ID, String trxName) {
		super(ctx, HSS_ProductSize_ID, trxName);
	}

	public MProductSize(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

}
