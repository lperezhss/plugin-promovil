/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.hss.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for HSS_RegAndroid
 *  @author iDempiere (generated) 
 *  @version Release 5.1 - $Id$ */
public class X_HSS_RegAndroid extends PO implements I_HSS_RegAndroid, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20191027L;

    /** Standard Constructor */
    public X_HSS_RegAndroid (Properties ctx, int HSS_RegAndroid_ID, String trxName)
    {
      super (ctx, HSS_RegAndroid_ID, trxName);
      /** if (HSS_RegAndroid_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_HSS_RegAndroid (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 2 - Client 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_HSS_RegAndroid[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Comment/Help.
		@param Help 
		Comment or Hint
	  */
	public void setHelp (String Help)
	{
		set_Value (COLUMNNAME_Help, Help);
	}

	/** Get Comment/Help.
		@return Comment or Hint
	  */
	public String getHelp () 
	{
		return (String)get_Value(COLUMNNAME_Help);
	}

	/** Set HSS_RegAndroid.
		@param HSS_RegAndroid_ID HSS_RegAndroid	  */
	public void setHSS_RegAndroid_ID (int HSS_RegAndroid_ID)
	{
		if (HSS_RegAndroid_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_HSS_RegAndroid_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_HSS_RegAndroid_ID, Integer.valueOf(HSS_RegAndroid_ID));
	}

	/** Get HSS_RegAndroid.
		@return HSS_RegAndroid	  */
	public int getHSS_RegAndroid_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_HSS_RegAndroid_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set HSS_RegAndroid_UU.
		@param HSS_RegAndroid_UU HSS_RegAndroid_UU	  */
	public void setHSS_RegAndroid_UU (String HSS_RegAndroid_UU)
	{
		set_ValueNoCheck (COLUMNNAME_HSS_RegAndroid_UU, HSS_RegAndroid_UU);
	}

	/** Get HSS_RegAndroid_UU.
		@return HSS_RegAndroid_UU	  */
	public String getHSS_RegAndroid_UU () 
	{
		return (String)get_Value(COLUMNNAME_HSS_RegAndroid_UU);
	}

	/** Pendiente = P */
	public static final String HSS_STATUS_Pendiente = "P";
	/** Certificado = C */
	public static final String HSS_STATUS_Certificado = "C";
	/** Administrador = A */
	public static final String HSS_STATUS_Administrador = "A";
	/** Inactivo = I */
	public static final String HSS_STATUS_Inactivo = "I";
	/** Desconocido = D */
	public static final String HSS_STATUS_Desconocido = "D";
	/** Vencido = V */
	public static final String HSS_STATUS_Vencido = "V";
	/** Set Estado.
		@param HSS_Status Estado	  */
	public void setHSS_Status (String HSS_Status)
	{

		set_Value (COLUMNNAME_HSS_Status, HSS_Status);
	}

	/** Get Estado.
		@return Estado	  */
	public String getHSS_Status () 
	{
		return (String)get_Value(COLUMNNAME_HSS_Status);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	public org.compiere.model.I_AD_User getSalesRep() throws RuntimeException
    {
		return (org.compiere.model.I_AD_User)MTable.get(getCtx(), org.compiere.model.I_AD_User.Table_Name)
			.getPO(getSalesRep_ID(), get_TrxName());	}

	/** Set Sales Representative.
		@param SalesRep_ID 
		Sales Representative or Company Agent
	  */
	public void setSalesRep_ID (int SalesRep_ID)
	{
		if (SalesRep_ID < 1) 
			set_Value (COLUMNNAME_SalesRep_ID, null);
		else 
			set_Value (COLUMNNAME_SalesRep_ID, Integer.valueOf(SalesRep_ID));
	}

	/** Get Sales Representative.
		@return Sales Representative or Company Agent
	  */
	public int getSalesRep_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SalesRep_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_ValueNoCheck (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}