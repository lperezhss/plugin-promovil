/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.hss.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for HSS_Cupon
 *  @author iDempiere (generated) 
 *  @version Release 5.1
 */
@SuppressWarnings("all")
public interface I_HSS_Cupon 
{

    /** TableName=HSS_Cupon */
    public static final String Table_Name = "HSS_Cupon";

    /** AD_Table_ID=1000017 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 2 - Client 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(2);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name HSS_Client */
    public static final String COLUMNNAME_HSS_Client = "HSS_Client";

	/** Set Client	  */
	public void setHSS_Client (int HSS_Client);

	/** Get Client	  */
	public int getHSS_Client();

    /** Column name HSS_Cupon_ID */
    public static final String COLUMNNAME_HSS_Cupon_ID = "HSS_Cupon_ID";

	/** Set HSS_Cupon	  */
	public void setHSS_Cupon_ID (int HSS_Cupon_ID);

	/** Get HSS_Cupon	  */
	public int getHSS_Cupon_ID();

    /** Column name HSS_Cupon_UU */
    public static final String COLUMNNAME_HSS_Cupon_UU = "HSS_Cupon_UU";

	/** Set HSS_Cupon_UU	  */
	public void setHSS_Cupon_UU (String HSS_Cupon_UU);

	/** Get HSS_Cupon_UU	  */
	public String getHSS_Cupon_UU();

    /** Column name HSS_Org */
    public static final String COLUMNNAME_HSS_Org = "HSS_Org";

	/** Set Org	  */
	public void setHSS_Org (int HSS_Org);

	/** Get Org	  */
	public int getHSS_Org();

    /** Column name HSS_Role */
    public static final String COLUMNNAME_HSS_Role = "HSS_Role";

	/** Set Role	  */
	public void setHSS_Role (int HSS_Role);

	/** Get Role	  */
	public int getHSS_Role();

    /** Column name HSS_SalesRep */
    public static final String COLUMNNAME_HSS_SalesRep = "HSS_SalesRep";

	/** Set SalesRep	  */
	public void setHSS_SalesRep (int HSS_SalesRep);

	/** Get SalesRep	  */
	public int getHSS_SalesRep();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name URL */
    public static final String COLUMNNAME_URL = "URL";

	/** Set URL.
	  * Full URL address - e.g. http://www.idempiere.org
	  */
	public void setURL (String URL);

	/** Get URL.
	  * Full URL address - e.g. http://www.idempiere.org
	  */
	public String getURL();

    /** Column name Value */
    public static final String COLUMNNAME_Value = "Value";

	/** Set Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value);

	/** Get Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public String getValue();
}
