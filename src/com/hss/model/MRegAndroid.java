package com.hss.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MRegAndroid extends X_HSS_RegAndroid {

	private static final long serialVersionUID = 3126040967751049269L;

	public MRegAndroid(Properties ctx, int HSS_RegAndroid_ID, String trxName) {
		super(ctx, HSS_RegAndroid_ID, trxName);
	}

	public MRegAndroid(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

}
