package com.hss.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MConfiguracionOrden extends X_HSS_ConfiguracionOrden {

	private static final long serialVersionUID = -173025547986482159L;

	public MConfiguracionOrden(Properties ctx, int HSS_ConfiguracionOrden_ID, String trxName) {
		super(ctx, HSS_ConfiguracionOrden_ID, trxName);
	}

	public MConfiguracionOrden(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

}
