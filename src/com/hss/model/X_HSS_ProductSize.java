/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.hss.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for HSS_ProductSize
 *  @author iDempiere (generated) 
 *  @version Release 5.1 - $Id$ */
public class X_HSS_ProductSize extends PO implements I_HSS_ProductSize, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20191027L;

    /** Standard Constructor */
    public X_HSS_ProductSize (Properties ctx, int HSS_ProductSize_ID, String trxName)
    {
      super (ctx, HSS_ProductSize_ID, trxName);
      /** if (HSS_ProductSize_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_HSS_ProductSize (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 2 - Client 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_HSS_ProductSize[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Product Size.
		@param HSS_ProductSize_ID Product Size	  */
	public void setHSS_ProductSize_ID (int HSS_ProductSize_ID)
	{
		if (HSS_ProductSize_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_HSS_ProductSize_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_HSS_ProductSize_ID, Integer.valueOf(HSS_ProductSize_ID));
	}

	/** Get Product Size.
		@return Product Size	  */
	public int getHSS_ProductSize_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_HSS_ProductSize_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set HSS_ProductSize_UU.
		@param HSS_ProductSize_UU HSS_ProductSize_UU	  */
	public void setHSS_ProductSize_UU (String HSS_ProductSize_UU)
	{
		set_ValueNoCheck (COLUMNNAME_HSS_ProductSize_UU, HSS_ProductSize_UU);
	}

	/** Get HSS_ProductSize_UU.
		@return HSS_ProductSize_UU	  */
	public String getHSS_ProductSize_UU () 
	{
		return (String)get_Value(COLUMNNAME_HSS_ProductSize_UU);
	}

	public I_HSS_Size getHSS_Size() throws RuntimeException
    {
		return (I_HSS_Size)MTable.get(getCtx(), I_HSS_Size.Table_Name)
			.getPO(getHSS_Size_ID(), get_TrxName());	}

	/** Set Size.
		@param HSS_Size_ID Size	  */
	public void setHSS_Size_ID (int HSS_Size_ID)
	{
		if (HSS_Size_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_HSS_Size_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_HSS_Size_ID, Integer.valueOf(HSS_Size_ID));
	}

	/** Get Size.
		@return Size	  */
	public int getHSS_Size_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_HSS_Size_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Product_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}