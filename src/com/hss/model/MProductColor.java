package com.hss.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MProductColor extends X_HSS_ProductColor {

	private static final long serialVersionUID = -3680226721261436186L;

	public MProductColor(Properties ctx, int HSS_ProductColor_ID, String trxName) {
		super(ctx, HSS_ProductColor_ID, trxName);
	}

	public MProductColor(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

}
