/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.hss.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for HSS_ConfiguracionOrden
 *  @author iDempiere (generated) 
 *  @version Release 5.1 - $Id$ */
public class X_HSS_ConfiguracionOrden extends PO implements I_HSS_ConfiguracionOrden, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20191027L;

    /** Standard Constructor */
    public X_HSS_ConfiguracionOrden (Properties ctx, int HSS_ConfiguracionOrden_ID, String trxName)
    {
      super (ctx, HSS_ConfiguracionOrden_ID, trxName);
      /** if (HSS_ConfiguracionOrden_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_HSS_ConfiguracionOrden (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 2 - Client 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_HSS_ConfiguracionOrden[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Document Type.
		@param C_DocType_ID 
		Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID)
	{
		if (C_DocType_ID < 0) 
			set_Value (COLUMNNAME_C_DocType_ID, null);
		else 
			set_Value (COLUMNNAME_C_DocType_ID, Integer.valueOf(C_DocType_ID));
	}

	/** Get Document Type.
		@return Document type or rules
	  */
	public int getC_DocType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_DocType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set HSS_ConfiguracionOrden.
		@param HSS_ConfiguracionOrden_ID HSS_ConfiguracionOrden	  */
	public void setHSS_ConfiguracionOrden_ID (int HSS_ConfiguracionOrden_ID)
	{
		if (HSS_ConfiguracionOrden_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_HSS_ConfiguracionOrden_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_HSS_ConfiguracionOrden_ID, Integer.valueOf(HSS_ConfiguracionOrden_ID));
	}

	/** Get HSS_ConfiguracionOrden.
		@return HSS_ConfiguracionOrden	  */
	public int getHSS_ConfiguracionOrden_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_HSS_ConfiguracionOrden_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set HSS_ConfiguracionOrden_UU.
		@param HSS_ConfiguracionOrden_UU HSS_ConfiguracionOrden_UU	  */
	public void setHSS_ConfiguracionOrden_UU (String HSS_ConfiguracionOrden_UU)
	{
		set_ValueNoCheck (COLUMNNAME_HSS_ConfiguracionOrden_UU, HSS_ConfiguracionOrden_UU);
	}

	/** Get HSS_ConfiguracionOrden_UU.
		@return HSS_ConfiguracionOrden_UU	  */
	public String getHSS_ConfiguracionOrden_UU () 
	{
		return (String)get_Value(COLUMNNAME_HSS_ConfiguracionOrden_UU);
	}

	public org.compiere.model.I_M_PriceList getM_PriceList() throws RuntimeException
    {
		return (org.compiere.model.I_M_PriceList)MTable.get(getCtx(), org.compiere.model.I_M_PriceList.Table_Name)
			.getPO(getM_PriceList_ID(), get_TrxName());	}

	/** Set Price List.
		@param M_PriceList_ID 
		Unique identifier of a Price List
	  */
	public void setM_PriceList_ID (int M_PriceList_ID)
	{
		if (M_PriceList_ID < 1) 
			set_Value (COLUMNNAME_M_PriceList_ID, null);
		else 
			set_Value (COLUMNNAME_M_PriceList_ID, Integer.valueOf(M_PriceList_ID));
	}

	/** Get Price List.
		@return Unique identifier of a Price List
	  */
	public int getM_PriceList_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_PriceList_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Warehouse getM_Warehouse() throws RuntimeException
    {
		return (org.compiere.model.I_M_Warehouse)MTable.get(getCtx(), org.compiere.model.I_M_Warehouse.Table_Name)
			.getPO(getM_Warehouse_ID(), get_TrxName());	}

	/** Set Warehouse.
		@param M_Warehouse_ID 
		Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID)
	{
		if (M_Warehouse_ID < 1) 
			set_Value (COLUMNNAME_M_Warehouse_ID, null);
		else 
			set_Value (COLUMNNAME_M_Warehouse_ID, Integer.valueOf(M_Warehouse_ID));
	}

	/** Get Warehouse.
		@return Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Warehouse_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}